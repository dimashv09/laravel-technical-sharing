<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'author',
        'id_category'
    ];

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'id_category');
    }
}
