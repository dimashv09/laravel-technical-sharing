@extends('layouts.app')

@section('content')
<h1>Tambah Artikel</h1>
<form action="{{ route('post-categories.store') }}" method="POST">
@csrf
  <div class="form-group">
    <label for="name">Judul</label>
    <input type="text" class="form-control" id="name" name="name">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection