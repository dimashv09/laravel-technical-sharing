@extends('layouts.app')

@section('content')
    <h1>Tabel Artikel</h1>
    @if($message = Session::get('success'))
    <div class="alert alert-success" role="alert">
        {{$message}}
    </div>
    @endif
    <a href="{{ route('posts.create') }}"><button type="button" class="btn btn-primary">Tambah Artikel</button></a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Judul Artikel</th>
                <th scope="col">Author</th>
                <th scope="col">Category</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->author}}</td>
                    <td>{{$post->category->name}}</td>
                    <td>
                        <form action="{{route('posts.destroy', $post->id)}}" method="POST">
                            <a href="{{route('posts.show', $post->id)}}" class="btn btn-success">Show</a>
                            <a href="{{route('posts.edit', $post->id)}}" class="btn btn-warning">Edit</a>
                            
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection