@extends('layouts.app')

@section('content')
<h1>{{ $post->title }}</h1>
<p>{{ $post->content }}</p>
<p>{{ $post->author }}</p>
<p>{{ $post->category->name }}</p>
@endsection