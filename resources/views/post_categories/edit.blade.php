@extends('layouts.app')

@section('content')
<h1>Tambah Artikel</h1>
<form action="{{ route('post-categories.update', $post_category->id) }}" method="POST">
@csrf
@method('PUT')
  <div class="form-group">
    <label for="name">Judul</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ $post_category->name  }}">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection