<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function getAllPost(){
        $posts = Post::all();

        return response()->json([
            'message' => $posts,
            'status' => 'success'
        ]);
    }

    public function postNewPost(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'author' => 'required',
            'id_category' => 'required|exists:post_categories,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'status' => 'error'
            ]);
        }

        $newPost = Post::create($request->all());
        
        if($newPost){
            return response()->json([
                'message' => 'Data berhasil ditambakan',
                'status' => 'success'
            ]);
        }else{
            return response()->json([
                'message' => 'Masalah Server',
                'status' => 'error'
            ]);
        }
    }

    public function updatePost(Request $request, $id){
        $post = Post::find($id);
        
        if($post){
            $validator = Validator::make($request->all(), [
                'id_category' => 'exists:post_categories,id'
            ]);
    
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->all(),
                    'status' => 'error'
                ]);
            }

            $updatePost = $post->update($request->all());

            if($updatePost){
                return response()->json([
                    'message' => 'Data berhasil diubah',
                    'status' => 'success'
                ]);
            }else{
                return response()->json([
                    'message' => 'Masalah Server',
                    'status' => 'error'
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Post tidak ditemukan',
                'status' => 'error'
            ]);
        }
    }

    public function destroyPost($id){
        $post = Post::find($id);

        if($post){
            $deletePost = $post->delete();

            if($deletePost){
                return response()->json([
                    'message' => 'Data berhasil dihapus',
                    'status' => 'success'
                ]);
            }else{
                return response()->json([
                    'message' => 'Masalah Server',
                    'status' => 'error'
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Post tidak ditemukan',
                'status' => 'error'
            ]);
        }
    }
}
