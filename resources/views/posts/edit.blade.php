@extends('layouts.app')

@section('content')
<h1>Tambah Artikel</h1>
<form action="{{ route('posts.update', $post->id) }}" method="POST">
@csrf
@method('PUT')
  <div class="form-group">
    <label for="title">Judul</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
  </div>
  <div class="form-group">
    <label for="content">Content</label>
    <input type="text" class="form-control" id="content" name="content" value="{{ $post->content }}">
  </div>
  <div class="form-group">
    <label for="author">Author</label>
    <input type="text" class="form-control" id="author" name="author" value="{{ $post->author }}">
  </div>
  <div class="form-group">
    <label for="category">Kategori</label>
    <select class="form-control" id="category" name="id_category">
      @foreach($post_categories as $post_category)
        @if($post_category->id == $post->id_category)
          <option value="{{ $post_category->id }}" selected>{{ $post_category->name }}</option>
        @else
          <option value="{{ $post_category->id }}">{{ $post_category->name }}</option>
        @endif
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection