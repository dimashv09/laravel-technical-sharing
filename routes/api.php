<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('posts-list', [PostController::class, 'getAllPost']);
Route::post('input-post', [PostController::class, 'postNewPost']);
Route::post('update-post/{id}', [PostController::class, 'updatePost']);
Route::delete('delete-post/{id}', [PostController::class, 'destroyPost']);
