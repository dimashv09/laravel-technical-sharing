@extends('layouts.app')

@section('content')
    <h1>Tabel Kategori Artikel</h1>
    @if($message = Session::get('success'))
    <div class="alert alert-success" role="alert">
        {{$message}}
    </div>
    @endif
    <a href="{{ route('post-categories.create') }}"><button type="button" class="btn btn-primary">Tambah Kategori</button></a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Kategori</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($post_categories as $post_category)
                <tr>
                    <td>{{$post_category->id}}</td>
                    <td>{{$post_category->name}}</td>
                    <td>
                        <form action="{{route('post-categories.destroy', $post_category->id)}}" method="POST">
                            <a href="{{route('post-categories.show', $post_category->id)}}" class="btn btn-success">Show</a>
                            <a href="{{route('post-categories.edit', $post_category->id)}}" class="btn btn-warning">Edit</a>
                            
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection